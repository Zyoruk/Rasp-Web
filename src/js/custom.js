$(document).ready(function () {
    // Grab elements, create settings, etc.
    var video = document.getElementById('video');

    // Get access to the camera!
    if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // Not adding `{ audio: true }` since we only want video now
        navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
            video.src = window.URL.createObjectURL(stream);
            video.play();
        });

    }

    setInterval(function (){
        getLightBulbs();
    } , 1000);

      // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href="#"]').click(function() {
        $('html, body').animate({
          scrollTop: 0
        }, 1000, "easeInOutExpo");
        return false;
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() > 300){
              $(".ui-to-top").fadeIn(600);
        }else
            $(".ui-to-top").fadeOut(400);
    });

    $("#blueprint-svg [id*=lbulb]").click(
        function () {
            let id = $(this).prop("id").substr($(this).prop("id").indexOf("-")+1);
            let ajax_url = "http://djenkins.tech:8081/light/";

            if ($(this).class("on")){
                ajax_url += "On";
            }else {
                ajax_url += "Off";
            }

            ajax_url += id;

            $.ajax({
                url: ajax_url,
                type:"get",
                success:function(response) {
                  //document.getElementById("total_items").value=response;
                 console.log (response);
               },
               error:function(response){
                console.warn (response);
               }

              });
        }
    );

    $(".stop-vid").click(function (){
        $.ajax({
            url:"http://djenkins.tech:8081/vid",
            type:"delete",
            success:function(response) {
              //document.getElementById("total_items").value=response;
             console.log (response);
           },
           error:function(response){
            console.warn (response);
           }

          });
    });
});

function offAll () {
    $.ajax({
        url:"http://djenkins.tech:8081/lightsOff",
        type:"post",
        success:function(response) {
            try {
                let jsonData = JSON.parse(response);
                console.dir(jsonData)
            } catch (error) {
                alert ("Error parsing response.")
            }

       },
       error:function(response){
        console.warn (response);
       }

      });

}

function onAll () {
    $.ajax({
        url:"http://djenkins.tech:8081/lightsOn",
        type:"post",
        success:function(response) {
            try {
                let jsonData = JSON.parse(response);
                console.dir(jsonData)
            } catch (error) {
                alert ("Error parsing response.")
            }

       },
       error:function(response){
        console.warn (response);
       }

      });

}

function getDoors() {
    $.ajax({
        url:"http://djenkins.tech:8081/doors",
        type:"get",
        success:function(response) {
            try {
                let jsonData = JSON.parse(response);
                console.dir(jsonData)
            } catch (error) {
                alert ("Error parsing response.")
            }

       },
       error:function(response){
        console.warn (response);
       }

      });
}

function getLightBulbs() {
    console.warn ('aqui ta');

    $.get('http://djenkins.tech:8081/lights', function(r1){
        console.warn ('aqui tax2');
        try {
            let jsonData = JSON.parse(r1);
            console.dir(jsonData);
        } catch (error) {
            console.warn('Error');
        }
    });

}

// $.post("url", data,
//     function (data, textStatus, jqXHR) {

//     },
//     "dataType"
// );
