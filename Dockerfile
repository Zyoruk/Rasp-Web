FROM ubuntu:latest

ENV DEBIAN_FRONTEND noninteractive

RUN apt update && apt install -y \
			nginx-full \
			vim
ADD conf/site.conf /etc/nginx/sites-available/default

COPY conf /conf/
COPY src /app/

# Mount here your custom config folder
VOLUME /conf
RUN chown -R www-data:www-data /app
RUN chmod -R 720 /app
CMD ["nginx", "-g", "daemon off;"]

